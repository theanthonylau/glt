# [1.2.0](https://gitlab.com/nickshine/glt/compare/v1.1.0...v1.2.0) (2019-04-22)


### Features

* add glt env stop task ([8e9a41c](https://gitlab.com/nickshine/glt/commit/8e9a41c))

# [1.1.0](https://gitlab.com/nickshine/glt/compare/v1.0.0...v1.1.0) (2019-04-12)


### Features

* add environment clean task ([424f3b4](https://gitlab.com/nickshine/glt/commit/424f3b4))

# [1.0.0](https://gitlab.com/nickshine/glt/compare/v0.0.1...v1.0.0) (2019-04-07)


### chore

* initial release ([0bbfc3a](https://gitlab.com/nickshine/glt/commit/0bbfc3a))


### BREAKING CHANGES

* initial v1.0.0 release
